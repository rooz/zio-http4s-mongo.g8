package $package$.services

import $package$.models.AssistantDAO
import org.mongodb.scala.MongoClient
import zio.Task
import zio.ZIO

object DatabaseService {

  def build(
    mongoClient: MongoClient,
    createMongoIndexes: Concurrency,
    database: DbName,
    databasePrefix: Option[String]
  ): Task[DAO] =
    for {
      fullDbName        <- buildDbName(database, databasePrefix)
      assistantDAO      <- AssistantDAO(mongoClient, createMongoIndexes, fullDbName)
    } yield {
      DAO(
        assistantDAO,
      )
    }

  private def buildDbName(
    database: DbName,
    databasePrefix: Option[String]
  ): Task[DbName] =
    databasePrefix
      .map(prefix => ZIO.fromEither(SettingsModels.toDatabase(s"$prefix$database")))
      .getOrElse(ZIO.succeed(database))
      .mapError(new RuntimeException(_))
}
