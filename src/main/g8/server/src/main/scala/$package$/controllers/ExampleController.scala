package $package$.controllers

import $package$.Main
import $package$.tapir.ZioSupport._
import io.circe.generic.JsonCodec
import org.http4s.HttpRoutes
import sttp.tapir._
import sttp.tapir.json.circe._
import zio.Task
import zio.ZIO

object ExampleController {


  private val errorOut: EndpointOutput[ErrorResponse] =
    oneOf(
      statusMapping(BadRequest, jsonBody[BadRequestError]),
      statusMapping(Conflict, jsonBody[AlreadyExistsError]),
      statusMapping(InternalServerError, jsonBody[ServerError]),
      statusMapping(NotFound, jsonBody[NotFoundError])
    )

  private val echo =
    endpoint.post
      .in("v1" / "$name$")
      .in(jsonBody[String])
      .out(jsonBody[String])
      .errorOut(errorOut)
      .tag("$name$")
      .summary("Example of requests")

  def endpoints = List(echo)

  def route(env: Main.Env): HttpRoutes[Task] =
    example.toZioRoutes(env) { name =>
        ZIO.succeed(s"Hello, $name")
    }

}

